package com.etiteam.rmiclient;

import com.etiteam.rmiclient.strategies.IStrategy;
import com.etiteam.rmiclient.strategies.impl.DumpingStrategy;
import com.etiteam.rmiclient.strategies.impl.LastMinuteStrategy;
import com.etiteam.rmiinterfaces.AuctionServiceConstants;
import com.etiteam.rmiinterfaces.IAuctionServer;
import com.etiteam.rmiinterfaces.IItem;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class MainWindowController {

	@FXML public ListView<IItem> itemsView;
	@FXML public ListView<String> registeredListenersView;
	@FXML public ListView<Map.Entry<String, IStrategy>> strategiesView;

	@FXML public TextField itemNameField;
	@FXML public TextField userNameField;

	@FXML public TextField itemDescriptionField;
	@FXML public TextField startBidField;
	@FXML public TextField auctionTimeField;

	@FXML public TextField bidField;
	@FXML public TextField maximumBidField;

	private static final Logger LOGGER = LoggerFactory.getLogger(MainWindowController.class);
	protected static final long ITEMS_REFRESH_PERIOD = 1000;

	protected final ObservableList<IItem> allItems = FXCollections.observableArrayList();
	protected final ObservableList<String> registeredListeners = FXCollections.observableArrayList();
	protected final ObservableList<Map.Entry<String, IStrategy>> strategies = FXCollections.observableArrayList();

	protected User user;
	protected Timer refreshingTimer;

	public MainWindowController() {
		user = createUser();
	}

	protected User createUser() {
		TextInputDialog dialog = new TextInputDialog("user1");
		dialog.setTitle("User name");
		dialog.setHeaderText("User name");
		dialog.setContentText("Please enter your user name:");

		Optional<String> result = dialog.showAndWait();
		return new User(result.get(), getAuctionServer());
	}

	@FXML
	public void initialize() {
		initRefreshingTimer();
		itemsView.setItems(allItems);
		registeredListenersView.setItems(registeredListeners);
		strategiesView.setItems(strategies);

		userNameField.setText(user.getName());
	}

	public void simpleButtonAction(ActionEvent actionEvent) {
		LOGGER.info("Button clicked");
	}

	protected IAuctionServer getAuctionServer() {
		try {
			Registry registry = LocateRegistry.getRegistry();
			return (IAuctionServer) registry.lookup(AuctionServiceConstants.SERVER_BIND_NAME);
		} catch (Exception e) {
			throw new RuntimeException("There is a problem with fetching bound IActionServer instance", e);
		}
	}

	public void dumpingStrategy(ActionEvent actionEvent) {
		try {
			double maximumBid = Double.parseDouble(maximumBidField.getText());
			IItem item = getItem(itemNameField.getText());
			IStrategy strategy = new DumpingStrategy(item, user, maximumBid);
			setStrategy(itemNameField.getText(), strategy);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void lastMinuteStrategy(ActionEvent actionEvent) {
		try {
			IItem item = getItem(itemNameField.getText());
			IStrategy strategy = new LastMinuteStrategy(item, user);
			setStrategy(itemNameField.getText(), strategy);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void removeStrategy(ActionEvent actionEvent) {
		setStrategy(itemNameField.getText(), null);
	}

	public void bid(ActionEvent actionEvent) {
		try {
			double bid = Double.parseDouble(bidField.getText());
			user.doBid(itemNameField.getText(), bid);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void registerListener(ActionEvent actionEvent) {
		try {
			String itemName = itemNameField.getText();
			user.doRegisteringListener(itemName);
			Platform.runLater(()-> registeredListeners.add(itemName));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void placeItem(ActionEvent actionEvent) {
		try {
			double startBid = Double.parseDouble(startBidField.getText());
			long auctionTime = Long.parseLong(auctionTimeField.getText());
			user.doPlacingItem(itemNameField.getText(), itemDescriptionField.getText(), startBid, auctionTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected IItem getItem(String itemName) {
		try {
			IItem item = getAuctionServer().getItems().get(itemName);
			if (item == null) {
				throw new RuntimeException(String.format("Item %s not found", itemName));
			}
			return item;
		} catch (Exception e) {
			throw new RuntimeException("Cannot get item " + itemName, e);
		}
	}

	protected void setStrategy(String itemName, IStrategy strategy) {
		user.setStrategy(itemName, strategy);
		Platform.runLater(() -> {
			strategies.clear();
			strategies.addAll(user.getStrategies().entrySet());
		});
	}

	protected void initRefreshingTimer() {
		if (refreshingTimer == null) {
			refreshingTimer = new Timer();
		} else {
			refreshingTimer.cancel();
		}

		refreshingTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					HashMap<String, IItem> newItems = getAuctionServer().getItems();
					updateItems(newItems);
				} catch (RemoteException ex) {
					LOGGER.error("Problem with updating items", ex);
				}
			}
		}, 0, ITEMS_REFRESH_PERIOD);
	}

	protected void updateItems(HashMap<String, IItem> newItems) {
		Platform.runLater(() -> {
			//HashSet<IItem> updatedItems = new HashSet<>();
			HashSet<IItem> deletedItems = new HashSet<>();

			// updating existing
			for (int i = 0; i < allItems.size(); ++i) {
				IItem item = allItems.get(i);
				IItem newItem = newItems.get(item.getName());
				if (newItem != null) {
					Collections.replaceAll(allItems, item, newItem);
					//		updatedItems.add(newItem);
				} else {
					deletedItems.add(item);
				}
			}
			allItems.removeAll(deletedItems);

			// adding new
			HashSet<IItem> createdItems = new HashSet<>(newItems.values());
			createdItems.removeAll(allItems);

			allItems.addAll(createdItems);
		});
	}
}