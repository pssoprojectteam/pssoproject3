package com.etiteam.rmiclient;

import com.etiteam.rmiclient.strategies.IStrategy;
import com.etiteam.rmiinterfaces.IAuctionListener;
import com.etiteam.rmiinterfaces.IAuctionServer;
import com.etiteam.rmiinterfaces.IItem;
import com.etiteam.rmiinterfaces.RemoteObservable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Observable;
import java.util.UUID;

public class User extends Observable implements IAuctionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(User.class);

	private String name;

	protected HashMap<String, IStrategy> strategies = new HashMap<>();

	protected IAuctionServer auctionServer;

	private IAuctionListener selfRmiStub;

	public User(String name, IAuctionServer auctionServer) {
		this.name = name;
		this.auctionServer = auctionServer;
		LOGGER.info("A new user({}) has been created", name);
	}

	public void doBid(String itemName, double bid) {
		try {
			LOGGER.info("{} outbid (bid={}) {}, ", this, bid, itemName);
			auctionServer.bidOnItem(name, itemName, bid);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public void doPlacingItem(String itemName, String itemDescription, double startBid, long auctionTime) {
		try {
			LOGGER.info("{} placed new item for bid: itemName={} startBid={} auctionTime={}",
				this, itemName, startBid, auctionTime);
			auctionServer.placeItemForBid(name, itemName, itemDescription, startBid, auctionTime);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public void doRegisteringListener(String itemName) {
		try {
			auctionServer.registerListener(getSelfRmiStub(), itemName);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public void setStrategy(String itemName, IStrategy strategy) {
		IStrategy previousStrategy = strategies.get(itemName);
		if (previousStrategy != null) {
			previousStrategy.stopExecuting();
			strategies.remove(itemName);
		}
		if (strategy != null) {
			strategies.put(itemName, strategy);
			strategy.startExecuting();
		}
	}

	private IAuctionListener getSelfRmiStub() {
		if (selfRmiStub == null) { // TODO: add synchronized block
			try {
				IAuctionListener stub = (IAuctionListener) UnicastRemoteObject.exportObject(this, 0);
				Registry registry = LocateRegistry.getRegistry();
				registry.rebind(UUID.randomUUID().toString(), stub);
				selfRmiStub = stub;
			} catch (RemoteException e) {
				throw new RuntimeException(String.format(
					"There was problem with creating IActionListener stub for user(%s)",
					getName()), e);
			}
		}
		return selfRmiStub;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HashMap<String, IStrategy> getStrategies() {
		return strategies;
	}

	public IAuctionServer getAuctionServer() {
		return auctionServer;
	}

	// DESIGN PATTERN: observer update method body
	@Override
	public void update(RemoteObservable o, Object arg) throws RemoteException {
		IItem newItem = (IItem) o;
		LOGGER.debug("[{}] Updated remotely item: {}, current bid: {}, time rem: {}",
			this, newItem.getName(), newItem.getCurrentBid(), newItem.getRemainingTime());

		setChanged();
		notifyObservers(newItem);
	}

	@Override
	public String toString() {
		return "User{" +
			"name='" + name + '\'' +
			'}';
	}
}
