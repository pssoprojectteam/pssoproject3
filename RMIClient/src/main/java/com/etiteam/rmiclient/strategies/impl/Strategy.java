package com.etiteam.rmiclient.strategies.impl;

import com.etiteam.rmiclient.User;
import com.etiteam.rmiclient.strategies.IStrategy;
import com.etiteam.rmiinterfaces.IItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Observable;

public abstract class Strategy implements IStrategy {

	private static final Logger LOGGER = LoggerFactory.getLogger(Strategy.class);

	protected IItem item;

	protected User user;

	protected boolean isEnabled;

	public Strategy(IItem item, User user) {
		this.item = item;
		this.user = user;
	}

	@Override
	public void startExecuting() {
		LOGGER.info("{} started Strategy on {}", user, item);
		isEnabled = true;
		user.addObserver(this);
	}

	@Override
	public void stopExecuting() {
		LOGGER.info("{} stopped Strategy on {}", user, item);
		isEnabled = false;
		user.deleteObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		IItem newItem = (IItem) arg;
		LOGGER.info("In {}'s strategy, updated item (\n\tfrom {} \n\tto {})", user, item, newItem);
		item = newItem;
	}
}
