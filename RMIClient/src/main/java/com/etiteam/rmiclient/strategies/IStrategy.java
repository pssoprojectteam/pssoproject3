package com.etiteam.rmiclient.strategies;

import java.util.Observer;

// DESIGN PATTERN: strategy interface
public interface IStrategy extends Observer {

	void startExecuting();

	void stopExecuting();
}
