package com.etiteam.rmiclient.strategies.impl;

import com.etiteam.rmiclient.User;
import com.etiteam.rmiinterfaces.IItem;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

// DESIGN PATTERN: strategy implementation
public class LastMinuteStrategy extends Strategy {

	private static final Logger LOGGER = LoggerFactory.getLogger(LastMinuteStrategy.class);

	private static final int MINUTE = 60 * 1000; // minute in milis

	private static final int OUTBID_AMOUNT_MULTIPLIER = 2;

	private Timer timer;

	public LastMinuteStrategy(IItem item, User user) {
		super(item, user);
	}

	@Override
	public void startExecuting() {
		super.startExecuting();
		refreshTimer(item);
	}

	@Override
	public void stopExecuting() {
		timer.cancel();
		super.stopExecuting();
	}

	@Override
	public void update(Observable o, Object arg) {
		IItem newItem = (IItem) arg;
		if (shouldItemBeUpdated(newItem)) {
			super.update(o, arg); // save new values before any changes (because of recursion)
			refreshTimer(newItem);
		} else {
			super.update(o, arg); // only save new values
		}
	}

	protected void refreshTimer(IItem actualItem) {
		if (timer == null) {
			timer = new Timer();
		} else {
			timer.cancel();
		}

		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				realizeStrategy();
			}
		}, getDelayTime(actualItem));
	}

	protected void realizeStrategy() {
		boolean isUserNotWinning = !StringUtils.equals(item.getWinningName(), user.getName());
		if (isEnabled && isUserNotWinning) {
			if (!isLessThanOneMinuteLeft()) {
				throw new RuntimeException("Wrong state");
			}

			final double bid = item.getCurrentBid() * OUTBID_AMOUNT_MULTIPLIER;
			LOGGER.info("Outbid auction {}, outbid amount {}", item, bid);
			user.doBid(item.getName(), bid);
		}
	}

	private boolean isLessThanOneMinuteLeft() {
		return item.getRemainingTime() < MINUTE && item.getRemainingTime() > 0;
	}

	protected boolean shouldItemBeUpdated(IItem newItem) {
		return item.getAuctionEndTime() != newItem.getAuctionEndTime();
	}

	protected long getDelayTime(IItem actualItem) {
		long delayTime = (actualItem.getAuctionEndTime() - MINUTE + 1) - System.currentTimeMillis();
		if (delayTime < 0) {
			delayTime = 0;
		}
		return delayTime;
	}

	@Override
	public String toString() {
		return "LastMinuteStrategy{" +
			"item=" + item +
			", user=" + user +
			", isEnabled=" + isEnabled +
			'}';
	}
}
