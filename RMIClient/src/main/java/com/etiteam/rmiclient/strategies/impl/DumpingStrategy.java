package com.etiteam.rmiclient.strategies.impl;

import com.etiteam.rmiclient.User;
import com.etiteam.rmiinterfaces.AuctionState;
import com.etiteam.rmiinterfaces.IItem;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Observable;

// DESIGN PATTERN: strategy implementation
public class DumpingStrategy extends Strategy {

	private static final Logger LOGGER = LoggerFactory.getLogger(DumpingStrategy.class);

	private static final double OUTBID_AMOUNT = 1.0;

	private double maximumBid;

	public DumpingStrategy(IItem item, User user, double maximumBid) {
		super(item, user);
		this.maximumBid = maximumBid;
	}

	@Override
	public void update(Observable o, Object arg) {
		IItem newItem = (IItem) arg;
		boolean auctionIsNotEnded = newItem.getAuctionState() != AuctionState.ENDED;
		boolean canBid = newItem.getCurrentBid() <= maximumBid - 1;
		boolean isUserNotWinning = !StringUtils.equals(newItem.getWinningName(), user.getName());

		if (isEnabled && shouldItemBeUpdated(newItem) && canBid && isUserNotWinning && auctionIsNotEnded) {
			super.update(o, arg); // saving before bidding (because of recursion)

			double bid = getNextBid(newItem);
			LOGGER.info("{} outbid (bid={}) {}. ", user, bid, newItem);
			user.doBid(newItem.getName(), bid);
		} else {
			super.update(o, arg); // only save new values
		}
	}

	protected double getNextBid(IItem actualItem) {
		return actualItem.getCurrentBid() + OUTBID_AMOUNT;
	}

	protected boolean shouldItemBeUpdated(IItem newItem) {
		return item.getCurrentBid() != newItem.getCurrentBid()
			|| !StringUtils.equals(item.getWinningName(), newItem.getWinningName());
	}

	@Override
	public String toString() {
		return "DumpingStrategy{" +
			"item=" + item +
			", user=" + user +
			", isEnabled=" + isEnabled +
			'}';
	}
}
