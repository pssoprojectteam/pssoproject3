package com.etiteam.rmiserver;

import com.etiteam.rmiinterfaces.IAuctionListener;
import com.etiteam.rmiinterfaces.IAuctionServer;
import com.etiteam.rmiinterfaces.IItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.HashMap;

public class AuctionServer implements IAuctionServer {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuctionServer.class);

	private HashMap<String, IItem> itemList = new HashMap<>();

	// DESIGN PATTERN: singleton
	private static AuctionServer singleton = new AuctionServer();

	public static AuctionServer instance() {
		return singleton;
	}

	@Override
	public void placeItemForBid(String ownerName, String itemName, String itemDescription, double startBid,
								long auctionTime) throws RemoteException {

		if (itemList.containsKey(itemName)) {
			throw new RuntimeException("The item " + itemName + "has been already placed. " +
					"A new item should have a unique name");
		}
		IItem item = new Item(ownerName, itemName, itemDescription,
			startBid, System.currentTimeMillis() + auctionTime);
		itemList.put(itemName, item);
		LOGGER.debug("Item {} has been successfully placed", item);
	}

	@Override
	public void bidOnItem(String bidderName, String itemName, double bid) throws RemoteException {
		Item item = (Item) itemList.get(itemName);
		if (item != null && item.getCurrentBid() < bid) {
			item.outbid(bidderName, bid);
			LOGGER.debug("User {} has successfully bid {}", bidderName, item);
			item.notifyObservers();
		} else {
			// message for server
			LOGGER.debug("User {} has not successfully bid {}", bidderName, item);

			// message for client
			throw new RuntimeException(String.format(
				"Problem with outbidding %s. Params: bidderName=%s, itemName=%s, bid=%f",
				item, bidderName, itemName, bid));
		}
	}

	@Override
	public HashMap<String, IItem> getItems() throws RemoteException {
		return itemList;
	}

	@Override
	public void registerListener(IAuctionListener iAuctionListener, String itemName) throws RemoteException {
        Item item = (Item) itemList.get(itemName);
		if (item != null) {
			LOGGER.debug("[{}}: {} has been successfully registered as a listener", itemName, iAuctionListener);
			item.addObserver(iAuctionListener);
		} else {
			String message = String.format(
				"{} has not been successfully registered as a {} listener", iAuctionListener, itemName);
			LOGGER.debug(message);                  // message for server
			throw new RuntimeException(message);    // message for client
		}
	}

    protected AuctionServer() {}
}
