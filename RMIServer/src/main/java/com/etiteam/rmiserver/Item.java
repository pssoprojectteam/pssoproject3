package com.etiteam.rmiserver;

import com.etiteam.rmiinterfaces.AuctionState;
import com.etiteam.rmiinterfaces.IItem;
import com.etiteam.rmiinterfaces.RemoteObservable;

public class Item extends RemoteObservable implements IItem {

	private String ownerName;

	private String name;

	private String description;

	private double currentBid;

	private long auctionEndTime;

	private String winningName;

	public Item(String ownerName, String name, String description, double currentBid, long endTime) {
		this.ownerName = ownerName;
		this.name = name;
		this.description = description;
		this.currentBid = currentBid;
		this.auctionEndTime = endTime;
		this.winningName = null;
	}

	public void outbid(String userName, double bid) {
		if (getAuctionState() != AuctionState.ACTIVE) {
			throw new RuntimeException("Outbid cannot be done, because the auction completed");
		}
		if (getCurrentBid() < bid) {
			setCurrentBid(bid);
			setWinningName(userName);
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public double getCurrentBid() {
		return currentBid;
	}

	@Override
	public long getAuctionEndTime() {
		return auctionEndTime;
	}

	@Override
	public long getRemainingTime() {
		return auctionEndTime - System.currentTimeMillis();
	}

	@Override
	public String getWinningName() {
		return winningName;
	}

	@Override
	public AuctionState getAuctionState() {
		if (getRemainingTime() <= 0) {
			return AuctionState.ENDED;
		} else {
			return AuctionState.ACTIVE;
		}
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCurrentBid(double currentBid) {
		this.currentBid = currentBid;
		setChanged();
	}

	public void setAuctionEndTime(long auctionEndTime) {
		this.auctionEndTime = auctionEndTime;
		setChanged();
	}

	public void setWinningName(String winningName) {
		this.winningName = winningName;
		setChanged();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Item item = (Item) o;

		if (Double.compare(item.getCurrentBid(), getCurrentBid()) != 0) return false;
		if (getAuctionEndTime() != item.getAuctionEndTime()) return false;
		if (!ownerName.equals(item.ownerName)) return false;
		if (!getName().equals(item.getName())) return false;
		if (getDescription() != null ? !getDescription().equals(item.getDescription()) : item.getDescription() != null)
			return false;
		if (getWinningName() != null ? !getWinningName().equals(item.getWinningName()) : item.getWinningName() != null)
			return false;
		return getAuctionState() == item.getAuctionState();
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = ownerName.hashCode();
		result = 31 * result + getName().hashCode();
		result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
		temp = Double.doubleToLongBits(getCurrentBid());
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + (int) (getAuctionEndTime() ^ (getAuctionEndTime() >>> 32));
		result = 31 * result + (getWinningName() != null ? getWinningName().hashCode() : 0);
		result = 31 * result + getAuctionState().hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "Item{" +
				"ownerName='" + ownerName + '\'' +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", currentBid=" + currentBid +
				", auctionEndTime=" + auctionEndTime +
				", winningName='" + winningName + '\'' +
				'}';
	}
}
