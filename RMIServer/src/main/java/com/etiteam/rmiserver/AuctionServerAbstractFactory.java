package com.etiteam.rmiserver;

import com.etiteam.rmiinterfaces.IAuctionServer;

// DESIGN PATTERN: abstract factory
public interface AuctionServerAbstractFactory {

	IAuctionServer createAuctionServer();
}
