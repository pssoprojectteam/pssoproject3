package com.etiteam.rmiserver;

import com.etiteam.rmiinterfaces.IAuctionServer;

// DESIGN PATTERN: factory
public class AuctionServerFactory implements AuctionServerAbstractFactory {

	@Override
	public IAuctionServer createAuctionServer() {
		return AuctionServer.instance();
	}
}
