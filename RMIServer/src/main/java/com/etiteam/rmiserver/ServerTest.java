package com.etiteam.rmiserver;

import com.etiteam.rmiinterfaces.IAuctionServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import static com.etiteam.rmiinterfaces.AuctionServiceConstants.SERVER_BIND_NAME;

public class ServerTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServerTest.class);

	private AuctionServerAbstractFactory actionServerFactory;

	public ServerTest(AuctionServerAbstractFactory auctionServerFactory) {
		this.actionServerFactory = auctionServerFactory;
	}

	public void run() throws RemoteException {
		IAuctionServer actionServer = useFactory();

		IAuctionServer stub = (IAuctionServer) UnicastRemoteObject.exportObject(actionServer, 0);
		Registry registry = LocateRegistry.getRegistry();
		registry.rebind(SERVER_BIND_NAME, stub);
	}

	protected IAuctionServer useFactory() {
		return actionServerFactory.createAuctionServer(); // creating instance by factory
	}

	public static void main(String[] args) {
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		ServerTest serverTest = new ServerTest(new AuctionServerFactory()); // init abstract factory
		try {
			serverTest.run();
			LOGGER.info("The simulation has been performed");
		} catch (RemoteException ex) {
			throw new RuntimeException("Exception has been thrown", ex);
		}
	}
}
