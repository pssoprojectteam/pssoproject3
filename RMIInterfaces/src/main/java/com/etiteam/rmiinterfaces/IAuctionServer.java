package com.etiteam.rmiinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

public interface IAuctionServer extends Remote {

	void placeItemForBid(String ownerName, String itemName,
			String itemDescription, double startBid, long auctionTime)
			throws RemoteException;

	void bidOnItem(String bidderName, String itemName, double bid)
			throws RemoteException;

	HashMap<String, IItem> getItems() throws RemoteException;

	void registerListener(IAuctionListener iAuctionListener, String itemName)
			throws RemoteException;
}
