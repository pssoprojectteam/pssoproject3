package com.etiteam.rmiinterfaces;

import java.rmi.RemoteException;
import java.util.Observer;
import java.util.Vector;

// DESIGN PATTERN: class that can be observable
public class RemoteObservable {
	private boolean changed = false;
	private Vector<RemoteObserver> obs;

	public RemoteObservable() {
		obs = new Vector<>();
	}

	public synchronized void addObserver(RemoteObserver o) {
		if (o == null)
			throw new NullPointerException();
		if (!obs.contains(o)) {
			obs.addElement(o);
		}
	}

	public synchronized void deleteObserver(RemoteObserver o) {
		obs.removeElement(o);
	}

	public void notifyObservers() throws RemoteException{
		notifyObservers(null);
	}

	public void notifyObservers(Object arg) throws RemoteException {
		Object[] arrLocal;

		synchronized (this) {
			if (!changed)
				return;
			arrLocal = obs.toArray();
			clearChanged();
		}

		for (int i = arrLocal.length-1; i>=0; i--)
			((RemoteObserver)arrLocal[i]).update(this, arg);
	}

	public synchronized void deleteObservers() {
		obs.removeAllElements();
	}

	protected synchronized void setChanged() {
		changed = true;
	}

	protected synchronized void clearChanged() {
		changed = false;
	}

	public synchronized boolean hasChanged() {
		return changed;
	}

	public synchronized int countObservers() {
		return obs.size();
	}
}
