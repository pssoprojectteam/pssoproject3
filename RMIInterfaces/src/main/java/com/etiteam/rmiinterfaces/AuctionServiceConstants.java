package com.etiteam.rmiinterfaces;

public final class AuctionServiceConstants {

	private AuctionServiceConstants() {}

	public static final String SERVER_BIND_NAME = "AuctionServer";
}
