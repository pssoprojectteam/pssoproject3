package com.etiteam.rmiinterfaces;

public enum AuctionState {
	ACTIVE,
	ENDED
}
