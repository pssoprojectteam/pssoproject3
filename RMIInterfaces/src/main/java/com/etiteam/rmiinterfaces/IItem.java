package com.etiteam.rmiinterfaces;

import java.io.Serializable;

public interface IItem extends Serializable {

	String getName();

	String getDescription();
	
	double getCurrentBid();

	long getAuctionEndTime();

	long getRemainingTime();

	String getWinningName();

	AuctionState getAuctionState();
}
