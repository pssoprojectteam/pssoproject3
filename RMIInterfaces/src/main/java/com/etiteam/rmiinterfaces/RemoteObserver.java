package com.etiteam.rmiinterfaces;

import java.rmi.RemoteException;

// DESIGN PATTERN: observer
public interface RemoteObserver {

	void update(RemoteObservable o, Object arg) throws RemoteException;
}
