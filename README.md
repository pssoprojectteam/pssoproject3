# Java-RMI - PSSO 3 project

## Project Setup

There are three subprojects: RMIClient, RMIServer, and RMIInterfaces.  These should be self-explanatory.  The server sets up a basic class to be used as a remote object.  The client then connects to it.  Since both the client and the server need to be able to know the definition of the interface, that is broken out into a separate project that they both have as a dependency.

## Running the project

Running this can be a somewhat involved task, but here's the simple way to do it:

1. Run the rmiregistry program.  This should be in $JAVA_HOME/bin.  ``rmiregistry -J-Djava.rmi.server.useCodebaseOnly=false``
    * To simplify use rmiserverRun.bat.

2. Run the RMIServer.  
    * Compile project by Maven ``mvn clean install``
    * Run the server with the following VM argumets: 
    ``-Djava.security.policy=policy -Djava.rmi.server.codebase=file:\pathToProject\RMIServe\target\RMIServer-1.0-SNAPSHOT-jar-with-dependencies.jar -Djava.rmi.server.useCodebaseOnly=false``
    * For Intelij we created a run configuration that do this
3. Run the RMIClient.  Make sure that when setting the RMI server codebase, the path ends with a /, otherwise **it will not work**.  Also ensure that client.policy is accessible by the application.  

    * VM arguments  
    ``-Djava.security.policy=policy -Djava.rmi.server.codebase=file:\pathToProject\RMIInterfaces\target\classes\ -Djava.rmi.server.useCodebaseOnly=false``
    * For Intelij we created a run configuration that do this
